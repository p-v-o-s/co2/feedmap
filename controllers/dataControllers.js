var db = require('../config/database');
//const sqlite = require("../src/aa-sqlite");
const fetch   = require('node-fetch');

require('dotenv').config({ path: __dirname + '/.env' })
var base_url = process.env['BASE_URL']

const getFeedDetails = (feedmap_pubkey) => db.query('SELECT * FROM feedmaps WHERE public_key = $1', [feedmap_pubkey]).then(response => response.rows[0])
.catch((err) => {
    console.log("couldn't find feedmap_id for that key!");
  });

exports.getPage = function(req, res, next) { // NOW BY PUB_KEY

    //var feedmap_pubkey = req.params.feedmap_pubkey;
    var feedmap_pubkey = 'a3ee855290463869dbb6f48721482fa52da7935054631078';

    console.log('getPage!');

     getFeedDetails(String(req.params.feedmap_pubkey))
     .then((feedmap_params) => {
        console.log(feedmap_params);
    res.render('data',{feedmap_pubkey:feedmap_params.public_key,feedmap_name:feedmap_params.name,map_url:feedmap_params.map_url,base_url:base_url});
     }).catch((err) => {
        console.log("Something went wrong with getPage!");
       });
}


const getFeedmapDetails = (feedmap_pubkey) => db.query('SELECT * FROM feedmaps WHERE public_key = $1', [feedmap_pubkey]).then(response => response.rows[0])
.catch((err) => {
    console.log("couldn't find feed_id for that key!");
  });


function handleErrors(response) {
    console.log(response);
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

exports.getFeedmap = function(req, res, next) {

    var public_key = req.params.feedmap_pubkey;
    
   
    getFeedmapDetails(public_key)
    .then((feedmap_params) => {
    
        console.log(feedmap_params);

        if(feedmap_params.length<2) {
            res.status(400).send("that didn't work!");
        }
        else{

    var insertSQL = 'SELECT * FROM feeds WHERE feedmap_id = $1';
    var params = [feedmap_params.feedmap_id];

    db.query(insertSQL, params, (error, result) => {
        if (error) {
            console.log(`Error: ${error}`)
            res.status(400).send(error);
        } else {

    console.log('query results:',result.rows);

    var feedmap_feeds = result.rows;

    //var feedmap_params = result.rows[0];

    //var feedmap_feeds = [{"feed_pubkey":"95irfkdk5hmk","name":"e9d7", "coords":{"x":-43, "y":48}},{"feed_pubkey":"iy4p6gmnmgym","name":"a00a","coords":{"x":-22, "y":55}}]
    
    //var feedmap_feeds =[];

    var colorBase = ["red","blue","green","purple","yellow"];

    colors=[];

    feedmap_feeds.forEach(function (value, i) {
        colors.push(colorBase[i%feedmap_feeds.length]);
        //console.log('%d: %s', i, value);
    });

    var allfeeddata = [];

    var itemsProcessed = 0;

    if (feedmap_feeds.length > 0 ) {
    feedmap_feeds.forEach(feed => {
        var feedkey = feed.feed_public_key;
        var shortname = feedkey.substring(0,3);
        var feedurl = 'http://co2data.pvos.org/data/'+feedkey+'/json/';
        console.log(feedurl);
        fetch(feedurl)
        .then(res => res.json())
        .then(data => {
            //allfeeddata.push(data);
            var thisitem = {"feed_nickname":feed.feed_nickname,"feedjson":data};
            
            allfeeddata.push(thisitem);
            itemsProcessed++;

            if(itemsProcessed=== feedmap_feeds.length) {
                //console.log(allfeeddata);

                //sort allfeeddata
                allfeeddata.sort(function(a, b) {
                    if (a.feedjson.feed_pubkey < b.feedjson.feed_pubkey) return -1;
                    if (a.feedjson.feed_pubkey > b.feedjson.feed_pubkey) return 1;
                    return 0;
                  });

                //console.log('first pubkey:');
                //console.log(allfeeddata[0].feed_pubkey);
                res.render('feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});
            }
        })
        .catch(err => {
            console.log(err);
            //res.status(200).send(error);
            res.render('feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});

        });

    });


}

    else {
        res.render('feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});
    }


        }
    })

}
}).catch(err => {
    console.log(err);
    res.status(400).send(error);
});


}
