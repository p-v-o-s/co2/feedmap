var db = require('../config/database');
require('dotenv').config({path: __dirname + '/../../.env'})
//var networkUtil = require('../utils/networkUtil');
const crypto = require("crypto");
const fetch   = require('node-fetch');

const { customAlphabet } = require('nanoid');
//const alphabet = 'abcdefghijklmnopqrstuvwxyz';
//const alphabet = '2345689abcdefghijkmnopqrstuvwxyz'
//const alphabet = 'abcdefghijkmnopqrstuvwxyz';
const alphabet = '23456789abcdefghijkmnpqrstuvwxyz' // 1000 IDs / hr, 12 char ID --> 17 years for 1/100 chance of collision


require('dotenv').config({ path: __dirname + '/.env' })
var base_url = process.env['BASE_URL']

console.log('base_url:',base_url);

const key_length = 12;
const nanoid = customAlphabet(alphabet, key_length);


const getFeedmapDetails = (feedmap_pubkey) => db.query('SELECT * FROM feedmaps WHERE public_key = $1', [feedmap_pubkey]).then(response => response.rows[0])
.catch((err) => {
    console.log("couldn't find feed_id for that key!");
  });


function handleErrors(response) {
    console.log(response);
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

exports.addFeed = function(req, res, next) {  // NOW BY PUB KEY

    console.log("adding new feed!");
    console.log(req.body);

    var feedurl = 'http://co2data.pvos.org/data/'+req.body.feed_pubkey+'/json/';
    var feed_nickname = req.body.feed_nickname;
    
    console.log(feedurl);
    console.log(feed_nickname);

    fetch(feedurl)
    .then(handleErrors)
    .then(function(response){
        
    

    getFeedmapDetails(String(req.params.feedmap_pubkey))
    .then((feedmap_params) => {

    console.log("feedmap_params:",feedmap_params);

    //console.log("got here!");
    // for use with the 'create_feed.pug' page
    // we need to add the additional parameters that Bayou expects
    //console.log("feedmap_pubkey:",req.params.feedmap_pubkey);
    //console.log("feed_pubkey:");
    //console.log(req.body.feed_pubkey);

    var feed_pubkey = String(req.body.feed_pubkey);
    
    var feedmap_id = feedmap_params.feedmap_id;
    
var dataValid = true; 


console.log("dataValid=",dataValid);
//res.status(200).send('Measurement recorded\n');
if (dataValid)  {
    // Create new measurement
    var insertSQL = `INSERT INTO feeds (feedmap_id, feed_public_key,feed_nickname) VALUES ($1, $2, $3);`
    var params = [feedmap_id, feed_pubkey, feed_nickname]

    db.query(insertSQL, params, (error, result) => {
        if (error) {
            res.status(400).send('adding feed failed!' );
        } else {
            //console.log(co2);
            //res.status(400).send("new feed added!");

            //allfeeddata=[];
            //res.render('manage_feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,private_key:feedmap_params.private_key,feedmap_name:feedmap_params.name,feeds:feedmap_feeds,base_url:base_url,colors:colors});
            //res.status(200).send('Measurement recorded\n');    
            var target_url = '/manage/'+feedmap_params.public_key.toString()+'/'+feedmap_params.private_key.toString()
            res.status(200).redirect(target_url);

        }
    });

} else {
    res.status(400).send('adding feed failed!' );
}

})
.catch((err) => {
    console.log("couldn't get parameters for that feed_id!");
    res.status(400).send('adding feed failed!' );

});

}).catch(function(error) {
    console.log(error);
    res.status(400).send('adding feed failed!' );
});


}

exports.postNewFeedmap = async function(req, res, next) {
    // Extract into variables from request body
    //var { feed_name} = req.body;
    var feedmap_name = req.body.feedmap_name;

    console.log(feedmap_name);
    
    
    // Check if the feed_name is valid

    let patt = /^[a-zA-Z0-9\s\-]+$/;
    var dataValid = (
        patt.test(feedmap_name)
    )
    
    if (dataValid)  {
        console.log('Valid feedname.');

        //var public_key = await (nanoid(alphabet,key_length));
        var public_key = await nanoid();
        var private_key = await nanoid();

        //var public_key = "8divvf9e7uz3";

        console.log(public_key,private_key);

        crypto.randomBytes(24, function(err, buffer) {
            //var private_key = buffer.toString('hex');
            
            crypto.randomBytes(24, function(err, buffer) {

                //var public_key = buffer.toString('hex');
            /*
            db.query('SELECT * FROM pg_catalog.pg_tables', function(err, result) {
                console.log("RESULTS:");
                console.log(result);
              });
              */

                var insertSQL = 'INSERT INTO feedmaps (name,public_key,private_key) VALUES ($1,$2,$3);'
                var params = [feedmap_name,public_key,private_key]
                db.query(insertSQL, params, (error, result) => {
                    if (error) {
                        console.log(`Error: ${error}`)
                        res.status(400).send(error);
                    } else {
                        console.log(`New feed '${feedmap_name}' created `);
                        console.log(`with key '${private_key}'.\n`);
                        //res.status(200).send(`New feed '${feed_name}' created with private_key '${private_key}'.\n`);
                        //var feed_id = req.params.feed_id;

                        //use the IP address for the feed link; change this once we have a fixed URL:
                        //var ip = networkUtil.getIp();
                        //var target_url = '/feedmap/manage/drkiazecxs6v/svjb8k36rwx9'
                        var target_url = '/manage/'+public_key.toString()+'/'+private_key.toString()
                        res.status(200).redirect(target_url);
                    }
                });
           });

        });
           
        
    
    } else {
        res.status(400).send('Feed name must be only letters, numbers, spaces, or dashes' );
    }

}


exports.getQR = function(req, res, next) {
  const file = `${__dirname}/upload-folder/dramaticpenguin.MOV`;
  console.log('hello');
  res.download(file); // Set disposition and send it.
};

exports.getFeedmapAdmin = function(req, res, next) {

    var public_key = req.params.feedmap_pubkey;
    var private_key = req.params.feedmap_privkey;

   
    getFeedmapDetails(public_key)
    .then((feedmap_params) => {

        console.log("feedmap_params:",feedmap_params);

        if(feedmap_params.length<2) {
            res.status(400).send("that didn't work!");
        }
        else{

            if(feedmap_params.private_key==req.params.feedmap_privkey) {

    console.log("key match!");

    var insertSQL = 'SELECT * FROM feeds WHERE feedmap_id = $1';
    var params = [feedmap_params.feedmap_id];

    db.query(insertSQL, params, (error, result) => {
        if (error) {
            console.log(`Error: ${error}`)
            res.status(400).send(error);
        } else {

    console.log('query results:',result.rows);

    var feedmap_feeds = result.rows;

    var colorBase = ["red","blue","green","purple","yellow"];

    colors=[];

    feedmap_feeds.forEach(function (value, i) {
        colors.push(colorBase[i%feedmap_feeds.length]);
        //console.log('%d: %s', i, value);
    });

    var allfeeddata = [];

    var itemsProcessed = 0;

    if (feedmap_feeds.length > 0 ) {
    feedmap_feeds.forEach(feed => {
        var feedkey = feed.feed_public_key;
        var shortname = feedkey.substring(0,3);
        var feedurl = 'http://co2data.pvos.org/data/'+feedkey+'/json/';
        console.log(feedurl);
        fetch(feedurl)
        .then(res => res.json())
        .then(data => {
            //allfeeddata.push(data);
            var thisitem = {"feed_nickname":feed.feed_nickname,"feedjson":data};
            allfeeddata.push(thisitem);
            itemsProcessed++;

            if(itemsProcessed=== feedmap_feeds.length) {
                //console.log(allfeeddata);

                //sort allfeeddata
                allfeeddata.sort(function(a, b) {
                    if (a.feedjson.feed_pubkey < b.feedjson.feed_pubkey) return -1;
                    if (a.feedjson.feed_pubkey > b.feedjson.feed_pubkey) return 1;
                    return 0;
                  });

                //console.log('first pubkey:');
                //console.log(allfeeddata[0].feed_pubkey);
                res.render('manage_feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,private_key:private_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});
            }
        })
        .catch(err => {
            console.log(err);
            //res.status(200).send(error);
            res.render('manage_feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,private_key:private_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});

        });

    });


}

    else {
        res.render('manage_feedmap',{bayoudata:allfeeddata,feedmap_pubkey:feedmap_params.public_key,private_key:private_key,feedmap_name:feedmap_params.name,base_url:base_url,colors:colors});
    }


        }
    })

}

 else {
        res.status(400).send("private key mismatch!");
    } 

        }
  

}).catch(err => {
    console.log(err);
    res.status(400).send(error);
});

}
