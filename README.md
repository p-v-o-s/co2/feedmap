# feed-map

# postgres setup
 
```
sudo apt install postgresql postgresql-contrib

sudo -i -u postgres

postgres@raspberrypi:~$ createdb feedmaps2
postgres@raspberrypi:~$ psql feedmaps2

CREATE TABLE feedmaps(
    feedmap_id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    public_key VARCHAR(255) UNIQUE,
    private_key VARCHAR(255),
    map_url VARCHAR(255)
);

CREATE TABLE feeds(
    id SERIAL PRIMARY KEY,
    feedmap_id INT,
    feed_base_url VARCHAR(255),
    feed_public_key VARCHAR(255),
    added TIMESTAMP DEFAULT NOW(),
    CONSTRAINT feedmap
        FOREIGN KEY(feedmap_id)
            REFERENCES feedmaps(feedmap_id)
);

\q
exit

psql

ALTER USER postgres with password '[PASSWORD]';

```
