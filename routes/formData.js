var express = require('express');
var router = express.Router();
var form_data_feeds = require('../controllers/formDataControllers');


router.get('/',form_data_feeds.getPage);
router.post('/addfeed/', form_data_feeds.addFeed);



module.exports = router;